# Demo 中文编程语言
展示如何使用 CovScript 从头打造一门简单的中文编程语言

https://zh-lang.osanswer.net/t/topic/138
## 安装依赖
```
cspkg install parsergen unicode ecs_bootstrap --yes
```
## 目录结构
 + unicode
   + utf8：由 UTF-8 编码的代码
     + imports：主程序
     + tests：测试
   + gbk：由 GBK 编码的代码
     + imports：主程序
     + tests：测试
 + misc：工具
 + cpl.ecs：主程序
 + cpl_genvisitor.csc：生成AST Visitor的帮助程序
## 如何运行
```bash
# 运行程序
ecs ./cpl.ecs run <*.cpl文件>
# 调试程序
ecs ./cpl.ecs debug <*.cpl文件>
```
cpl.ecs 能根据平台自动选择 GBK 或 UTF-8 编码
