var unicode_path = system.is_platform_windows() ? "./unicode/gbk/imports/" : "./unicode/utf8/imports/"
var cpl = context.source_import(unicode_path + "cpl_grammar.csp")
import visitorgen

var ofs = iostream.ofstream(unicode_path + "cpl_ast_visitor.csp")
(new visitorgen.visitor_generator).run(ofs, cpl.grammar.stx)
